#include <stdio.h>
#include <stdint.h>

#include "network/tcp.hpp"
#include "scheduler_task.hpp"

/* Kernel includes. */
#include <FreeRTOS.h>
#include "task.h"
#include "queue.h"
#include "semphr.h"


#include "trcUser.h"

#if (configUSE_TIMERS == 1)
    #include "timers.h"
#endif


static void enableFlushAfterPrintf()
{
    setvbuf(stdout, 0, _IONBF, 0);
    setvbuf(stdin, 0, _IONBF, 0);
}



#include "queue.h"
#include "semphr.h"

SemaphoreHandle_t one_Hz_sem;
QueueHandle_t data_q_handle;

QueueSetHandle_t data_or_sem_qs_handle;

void producer_1Hz_sem(void *p)
{
    while (1) {
        vTaskDelay(1000);
        xSemaphoreGive(one_Hz_sem);
    }
}

void producer_data(void *p)
{
    int x = 0;

    while (1) {
        ++x;
        xQueueSend(data_q_handle, &x, 0);
        vTaskDelay((rand() % 1000) + 100);
    }
}

void processor(void *p)
{
    int samples[10];
    int count = 0;

    while (1) {
        // DO NOT DO THIS:
        // xSemaphoreTake(one_Hz_sem);

        QueueSetMemberHandle_t who_unblocked = xQueueSelectFromSet(data_or_sem_qs_handle, 2000);
        if (who_unblocked == one_Hz_sem) {
            if (xSemaphoreTake(one_Hz_sem, 0)) {
                float avg = 0;
                for (int i = 0; i < count; i++) {
                    avg += samples[i];
                }
                avg /= count;
                count = 0;
                printf("1Hz: Average of samples = %0.1f\n", avg);
            }
            else {
                puts("ERROR, should never happen");
            }
        }
        else if (who_unblocked == data_q_handle) {
            int x =0;
            if (xQueueReceive(data_q_handle, &x, 0)) {
                samples[count++] = x;
                printf("Retrieved %i\n", x);
            }
            else {
                puts("ERROR, should never happen");
            }
        }
        else {
            puts("Invalid case");
        }

    }
}

int main_queueset(void)
{
    enableFlushAfterPrintf();

    one_Hz_sem = xSemaphoreCreateBinary();
    data_q_handle = xQueueCreate(10, sizeof(int));

    // Make sure that the queues or sems you are absorbing are empty
    data_or_sem_qs_handle = xQueueCreateSet(11);

    // Associate the semaphore and queue to the queue set handle
    xQueueAddToSet(one_Hz_sem, data_or_sem_qs_handle);
    xQueueAddToSet(data_q_handle, data_or_sem_qs_handle);

    xTaskCreate(producer_1Hz_sem, "task1",          1000, NULL, 1, NULL);
    xTaskCreate(producer_data, "task2 ",         1000, NULL, 1, NULL);
    xTaskCreate(processor, "processor",  1000, NULL, 2, NULL);
    vTaskStartScheduler();

    return 0;
}
