#ifndef UDP_CLASS
#define UDP_CLASS
#include "winsock2.h"
#include <windows.h>
#include <vector>

/** 

Simple UDP Class example to send/receive data
@code
	UDP client(1234);
	if(client.isConnected())
	{
		printf("UDP Client with IP: %s is listening at port %i\n", 
						client.getMyIP(), client.getMyPort() );

		char data[128] = "Me: Welcome message";
		client.sendDataTo("127.0.0.1", 5678, data, (int)strlen(data));

		// Wait 3 second for data to be available
		if(clients.isDataAvailable(3 *1000*1000))
		{
			if(client.getData(buff, sizeof(data)) > 0)
			{
				// Now you can use sendData() which will send data to 
				// the last connection which getData() got data from:
				client.sendData(data, (int)strlen(data));
			}
		}
	}
@endcode

Multipoint UDP
@code
	UDP_Multipoint udp(1234);
	if(udp.isConnected())
	{
		printf("UDP Client with IP: %s is listening at port %i\n",  udp.getMyIP(), udp.getMyPort() );

		// Start saving a list of all connections that send us data
		udp.startSavingClientList();

		// Wait for 3 clients to send data
		char buff[1500] = {0};
		while(3 != udp.getClientCount())
		{
			if(!udp.isDataAvailable(1 *1000*1000)
			{
				if(udp.getData(buff, sizeof(buff)) > 0)
				{
				}
			}
		}

		// Send data to all the clients who sent us the data.
		udp.sendDataToAllClients(buff, sizeof(buff));
	}
@endcode
*/

class UDP;
class UDP_Multipoint;

class UDP
{
public:
	UDP(int listenPort);
	virtual ~UDP();

	bool isConnected();               ///< Returns true if successful connection to UDP Socket
	char* getMyIP();                  ///< Returns the server IP as a char* string
	int   getMyPort();                ///< Returns the server Port for incoming data.
	char* getLastErrorDescription();  ///< Returns the last error description as char* string

	/// Sends the data to the LAST client that sent data to this UDP server
	/// Note: getData() should've been successful for this to work.
	/// @param data     The char* pointer to the data to send
	/// @param length   The length of the data bytes to send.
	/// @returns        true if data was sent successfully.
	bool sendData(char* data, int length);
	
	/// Sends data to specified IP address and port.
	/// @param IP      The IP Address as a string, such as "127.0.0.1"
	/// @param port    The Port of the other Peer to send data to
	/// @param data    The char* pointer to the data to send
	/// @param length  The length of the data bytes to send.
	/// @returns       true if at least some bytes defined by length were fully sent.
	bool sendDataTo(const char* IP, int port, const char* data, int length);

	/// @returns true if data is available on the server socket
	/// @param uSec   Time in micro-seconds to wait for data to be available.
	/// Note: the uSec will block this function until data is available up to the defined micro-seconds.
	bool isDataAvailable(int uSec);       

	/// Gets the data from the UDP Socket
	/// @param data    The char* pointer to the data to receive
	/// @param length  The length of the data bytes to receive
	/// @returns The total number of bytes received.
	int  getData(char* data, int length); 

private:
	WSADATA w;						/* Used to open windows connection */
	bool error;

	struct sockaddr_in server;		/* Information about the server */
	unsigned short listenPort;		/* Port of this UDP instance */
	char serverIP[32];				/* Stores server IP as string: xxx.xxx.xx.xx */
	char errorText[128];			/* Stores descriptive error text */
	bool clientIsKnown;				/* True if sendData() function has a destination */

	void setupServer(int port);     /* Called by constructor */

	// Empty function for inheriting class to over-ride to handle multi-point UDP connections.
	virtual void handleMultiPoint(struct sockaddr_in *newClient) {}

protected:
	struct sockaddr_in lastClient;	/* Information about the client */
	SOCKET sd;						/* Socket descriptor of server */
	int client_length;				/* Length of client struct */
};




class UDP_Multipoint 
	: public UDP
{
public:
	UDP_Multipoint(int listenPort);
	~UDP_Multipoint();

	/// Starts to save all clients into a list (incoming connections)
	/// This must be done to keep track of all incoming clients and distributeData()
	void startSavingClientList();  

	/// Clears all the client list.
	void clearClientList();        

	/// Returns the total number of unique clients that sent data on this UDP
	int  getClientCount();         

	/// Distributes the data to all clients, except for the last client that sent the data.
	void distributeData(char* data, int length);

	/// Sends the data to all the clients that have connected to this server.
	void sendDataToAllClients(char* data, int length);

private:	
	void handleMultiPoint(struct sockaddr_in *newClient);

	/* Begins to save list of all clients sending data to this UDP instance */
	bool saveClientList;				
	std::vector<struct sockaddr_in> clientList;
};

#endif
