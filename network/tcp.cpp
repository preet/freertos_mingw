#include "tcp.hpp"
#include <stdio.h>
#include <string.h>

TCP::TCP()
{
	setup();
}
TCP::~TCP()
{
	closesocket(ListenSocket);
	WSACleanup();
}

void TCP::setup()
{
	char host_name[256];		/* Name of the server */
	struct hostent *hp;			/* Information about this computer */

	error = false;
	serverIsUp = false;
	listenPort = 0;
	memset(serverIP, 0, sizeof(serverIP));
	memset(errorText, 0, sizeof(errorText));

	/* Open windows connection */
	if (WSAStartup(0x0202, &w) != 0)
	{
		strcpy(errorText, "Error opening windows connection");
		error = 1;
		return;
	}

	/* Get host name of this computer */
	gethostname(host_name, sizeof(host_name));
	hp = gethostbyname(host_name);

	/* Check for NULL pointer */
	if (hp == NULL)
	{
		strcpy(errorText, "Error getting host information");
		error = 1;
		return;
	}

	/* Save server IP */
	sprintf(serverIP, "%u.%u.%u.%u", (unsigned char)hp->h_addr_list[0][0],
											  (unsigned char)hp->h_addr_list[0][1],
											  (unsigned char)hp->h_addr_list[0][2],
											  (unsigned char)hp->h_addr_list[0][3]);
}

bool TCP::hostServer(int listenPort)
{
	if(error || serverIsUp)
	{
		return false;
	}

	this->listenPort = listenPort;
	// Create a SOCKET for listening for incoming connection requests.
	ListenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (ListenSocket == INVALID_SOCKET) 
	{
		sprintf(errorText, "Error creating socket (%i)", WSAGetLastError());
		return false;
	}

	// The sockaddr_in structure specifies the address family,
	// IP address, and port for the socket that is being bound.
	sockaddr_in service;
	service.sin_family = AF_INET;
	service.sin_port   = htons(listenPort);
	service.sin_addr.s_addr = inet_addr(serverIP);
	// Can optionally assign local IP address (any Ethernet adapter??)
	service.sin_addr.S_un.S_addr = INADDR_ANY;

	if (bind( ListenSocket, (SOCKADDR*) &service, sizeof(service)) == SOCKET_ERROR) 
	{
		sprintf(errorText, "Socket BIND failed (%i)", WSAGetLastError());
		closesocket(ListenSocket);
		return false;
	}

	// Listen for incoming connection requests on the created socket
	const int maxConnectionsQueue = 1;
	if (listen( ListenSocket, maxConnectionsQueue) == SOCKET_ERROR) 
	{
		sprintf(errorText, "Error listening on socket (%i)", WSAGetLastError());
		closesocket(ListenSocket);
		return false;
	}

	serverIsUp = true;
	return true;
}

TCP_TRANSCEIVER* TCP::acceptClientConnection()
{
	if(!serverIsUp)
	{
		return 0;
	}

	// Create a SOCKET for accepting incoming requests.
	SOCKET newSocket = accept( ListenSocket, NULL, NULL );
	if (newSocket == INVALID_SOCKET) 
	{
		sprintf(errorText, "Accept Connection Failed (%i)", WSAGetLastError());
		return 0;
	}

	TCP_TRANSCEIVER *connection = new TCP_TRANSCEIVER(newSocket);
	return connection;
}

TCP_TRANSCEIVER* TCP::connectAsClient(char* serverIP, int serverPort)
{
	struct sockaddr_in clientService; 

    // Create a SOCKET for connecting to server
    SOCKET ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (ConnectSocket == INVALID_SOCKET) 
	{
        sprintf(errorText, "Socket creation failed (%i)", WSAGetLastError());
		return 0;
    }

    // The sockaddr_in structure specifies the address family,
    // IP address, and port of the server to be connected to.
    clientService.sin_family = AF_INET;
    clientService.sin_addr.s_addr = inet_addr(serverIP);
    clientService.sin_port = htons( serverPort );

    // Connect to server.
    int iResult = connect( ConnectSocket, (SOCKADDR*) &clientService, sizeof(clientService) );
    if (iResult == SOCKET_ERROR) 
	{
		sprintf(errorText, "Connect failed with error(%i)", WSAGetLastError());
        closesocket(ConnectSocket);
        return 0;
	}

	TCP_TRANSCEIVER *connection = new TCP_TRANSCEIVER(ConnectSocket);
	return connection;
}

char* TCP::getIPAddress()
{
	return serverIP;
}
int   TCP::getServerPort()
{
	return this->listenPort;
}
char* TCP::getLastErrorText()
{
	return errorText;
}














TCP_TRANSCEIVER::TCP_TRANSCEIVER(SOCKET skt)
{
	thisSocket = skt;
	closed = false;
}

TCP_TRANSCEIVER::~TCP_TRANSCEIVER()
{
	closeConnection();
}

void TCP_TRANSCEIVER::closeConnection()
{
	if(!closed)
	{
		closesocket(thisSocket);
		closed = true;
	}
}

bool TCP_TRANSCEIVER::isDataAvailable(int uSec)
{
	if(closed) {
		return false;
	}
	
	struct timeval timeout;
	timeout.tv_sec  = (uSec / 1000000);
	timeout.tv_usec = (uSec % 1000000);

	struct fd_set sds;
	FD_ZERO(&sds);
    FD_SET(thisSocket, &sds);

	unsigned int val = select(0, &sds, 0, 0, &timeout);
	(void) val; // remove warning

	bool avail = (FD_ISSET(thisSocket, &sds)) ? true : false;
	return avail;
}

bool TCP_TRANSCEIVER::sendData(char* data, int len)
{
	if(closed) {
		return false;
	}

	int iResult = send( thisSocket, data, len, 0 );
    if (iResult == SOCKET_ERROR) 
	{
        closesocket(thisSocket);
		closed = true;
		return false;
    }
	return true;

}

int TCP_TRANSCEIVER::getData(char* data, int len)
{
	if(closed) {
		return 0;
	}

	int iResult = recv(thisSocket, data, len, 0);
    if ( iResult == 0 )
        closed = true;
    else
        sprintf(errorText, "Receive failed with error: %d\n", WSAGetLastError());

	return iResult;

}



bool TCP_TRANSCEIVER::isClosed()
{
	return closed;
}

bool TCP_TRANSCEIVER::isOpen()
{
	return !closed;
}

char* TCP_TRANSCEIVER::getLastErrorText()
{
	return errorText;
}

/*
bool TCP_TRANSCEIVER::getPeerInfo()
{
	if(closed) {
		return false;
	}
	
	struct sockaddr addr;
	int addrLen = sizeof(struct sockaddr);
	getpeername(thisSocket, &addr, &addrLen); 

}
*/
