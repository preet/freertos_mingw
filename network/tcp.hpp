#ifndef TCP_CLASS
#include "winsock2.h"
#include <windows.h>

class TCP;
class TCP_TRANSCEIVER;

/** Code to host a TCP/IP Server and exchange data:
@code
TCP tcp;
tcp.hostServer(13800);

TCP_TRANSCEIVER *connection = tcp.acceptClientConnection();
char hello[] = "Hello, say something\n";
connection->sendData(hello, sizeof(hello));

while( !connection->isDataAvailable(1 * 1000*1000)));
char data[128];
connection->getData(data, sizeof(data));
connection->closeConnection();
delete connection;
@endcode 
*/

/** Code to connect to TCP/IP Server as Client:
@code
TCP tcp;
TCP_TRANSCEIVER *connection = tcp.connectAsClient("127.0.0.1", 13801);
		
if(0 == connection)
{
	printf("Error connecting, possibly no server out there!\n");
	exit(0);
}

char hello[] = "Hello, I am your client, say something\n";
connection->sendData(hello, sizeof(hello));

while(!connection->isDataAvailable(1 * 1000*1000))
char data[128];
connection->getData(data, sizeof(data));

connection->closeConnection();
delete connection;
@endcode
*/

/// TCP class for:
///   1.  Setup a TCP/IP Server, and accept a TCP/IP Connection (Client)
///   2.  Connect to a TCP/IP Server
/// Communication is handled by TCP_TRANSCEIVER class.
class TCP
{
public:
	TCP();
	~TCP();

	/// Sets up the TCP Server on the given port
	/// @returns true if host server succeeds at the given port.
	/// NOTE: hostServer can only be called once, acceptClientConnection() can be called multiple times.
	bool hostServer(int listenPort);

	/// \PRE: hostServer() must be called prior to accepting a connection.
	/// Accepts a connection from client and returns the pointer of TCP_TRANSCEIVER
	/// to use for all future data exchange.
	TCP_TRANSCEIVER* acceptClientConnection();

	/// Connects to a TCP Server as a client.
	/// returns the pointer of TCP_TRANSCEIVER to use for all future data exchange.
	TCP_TRANSCEIVER* connectAsClient(char* serverIP, int serverPort);

	char* getIPAddress();       ///< Returns the IP of your computer as a string
	int   getServerPort();      ///< Returns the server Port (if hosting a server)
	char* getLastErrorText();	///< Returns the last error description as string

private:
	WSADATA w;				/* Used to open windows connection */
	SOCKET ListenSocket;
	
	bool serverIsUp;
	bool error;
	int  listenPort;
	char serverIP[32];		/* Stores server IP as string: xxx.xxx.xx.xx */
	char errorText[128];	/* Stores descriptive error text */

	void setup();
};









/// This is a socket instance returned by TCP class
/// Once a connection is established by TCP class, this
/// class object is used to exchange further data.
class TCP_TRANSCEIVER
{
public:
	bool isDataAvailable(int uSec);         ///< Returns true if there is data to be read.
	bool sendData(char* data, int len);     ///< Send the data to this connection, true if successful
	int   getData(char* data, int len);     ///< Get the data from this connection, @returns # bytes sent.
	
	void closeConnection();                 ///< Closes the connection to halt further communication
	bool isClosed();                        ///< Returns true if connection is closed/disconnected
	bool isOpen();                          ///< Returns true if connection is open/active
	char* getLastErrorText();	            ///< Returns the last error description as string
	
	~TCP_TRANSCEIVER();                     /// Destroys, disconnects the socket

private:
	/// Private constructor takes a socket that should already be connected.
	TCP_TRANSCEIVER(SOCKET skt);

	/// Allow TCP class to access the private constructor.
	friend class TCP;

	SOCKET thisSocket;
	bool closed;
	char errorText[128];	/* Stores descriptive error text */
};


#define TCP_CLASS
#endif