#include "udp.hpp"
#include <string.h>
#include <stdio.h>

UDP::UDP(int rxPort)
{
	setupServer(rxPort);
}

UDP::~UDP()
{
	closesocket(sd);
	WSACleanup();
}

void UDP::setupServer(int port)
{
	char host_name[256];		/* Name of the server */
	struct hostent *hp;			/* Information about this computer */
	
	clientIsKnown = false;
	error = 0;
	listenPort = port;

	memset(serverIP, 0, sizeof(serverIP));
	memset(errorText, 0, sizeof(errorText));

	/* Open windows connection */
	if (WSAStartup(0x0202, &w) != 0)
	{
		strcpy(errorText, "Error opening windows connection");
		error = 1;
		return;
	}

	/* Open a datagram socket */
	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sd == INVALID_SOCKET)
	{
		strcpy(errorText, "Error creating socket");
		error = 1;
		return;
	}

	/* Clear out server struct */
	memset((void *)&server, '\0', sizeof(struct sockaddr_in));

	/* Set family and port */
	server.sin_family = AF_INET;
	server.sin_port = htons(listenPort);

	/* Set address automatically if desired */
	/* Get host name of this computer */
	gethostname(host_name, sizeof(host_name));
	hp = gethostbyname(host_name);

	/* Check for NULL pointer */
	if (hp == NULL)
	{
		strcpy(errorText, "Error getting host name");
		error = 1;
		closesocket(sd);
	}
	
	/* Assign the address */
	server.sin_addr.S_un.S_un_b.s_b1 = hp->h_addr_list[0][0];
	server.sin_addr.S_un.S_un_b.s_b2 = hp->h_addr_list[0][1];
	server.sin_addr.S_un.S_un_b.s_b3 = hp->h_addr_list[0][2];
	server.sin_addr.S_un.S_un_b.s_b4 = hp->h_addr_list[0][3];

	/* Print out server information */
	sprintf(serverIP, "%u.%u.%u.%u", (unsigned char)server.sin_addr.S_un.S_un_b.s_b1,
											  (unsigned char)server.sin_addr.S_un.S_un_b.s_b2,
											  (unsigned char)server.sin_addr.S_un.S_un_b.s_b3,
											  (unsigned char)server.sin_addr.S_un.S_un_b.s_b4);

	// Can optionally assign local IP address (any Ethernet adapter??)
	server.sin_addr.S_un.S_addr = INADDR_ANY;

	/*
	if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char*)&bOptVal, sizeof(int)) != SOCKET_ERROR) 
	{
		printf("Set SO_REUSEADDR: ON\n");
	}
	if (setsockopt(sd, SOL_SOCKET, SO_BROADCAST, (char*)&bOptVal, sizeof(int)) != SOCKET_ERROR) 
	{
		printf("Set SO_BROADCAST: ON\n");
	}
	if (getsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char*)&bOptVal, &iOptLen) != SOCKET_ERROR)
	{
		printf("SO_REUSEADDR Value: %ld\n", bOptVal);
	}
	*/

	/* Bind address to socket */
	if (bind(sd, (struct sockaddr *)&server, sizeof(struct sockaddr_in)) == -1)
	{
		sprintf(errorText, "Could not bind to socket (%i)", WSAGetLastError());
		closesocket(sd);
		error = 1;
	}

	client_length = (int)sizeof(struct sockaddr_in);
}

bool UDP::isConnected()
{
	return !error;
}

char* UDP::getMyIP()
{
	return serverIP;
}

int UDP::getMyPort()
{
	return listenPort;
}

char* UDP::getLastErrorDescription()
{
	return errorText;
}

/*
void UDP::setTransmitDestination(char *IP, int transmitPort)
{
	clientIsKnown = true;
	// Copy server structure and change Port and IP.
	memcpy((char*)&lastClient, (char*)&server, sizeof(lastClient));
	lastClient.sin_addr.s_addr = inet_addr(IP);
	lastClient.sin_port = htons(transmitPort);
}
*/

bool UDP::sendData(char* data, int length)
{
	if(!clientIsKnown)
		return false;
	else
	{
		return (sendto(sd, data, length, 0, (struct sockaddr *)&lastClient, client_length) == length);
	}
}

bool UDP::sendDataTo(const char* IP, int port, const char* data, int length)
{
	sockaddr_in theClient;
	// Copy server structure and change Port and IP.
	memcpy((char*)&theClient, (char*)&server, sizeof(theClient));
	theClient.sin_addr.s_addr = inet_addr(IP);
	theClient.sin_port = htons(port);
	
	int bytesSent = sendto(sd, data, length, 0, (struct sockaddr *)&theClient, client_length);
	return (bytesSent > 0);
}

bool UDP::isDataAvailable(int uSec)
{
	if(error)
	{
		return 0;
	}

	struct timeval timeout;
	timeout.tv_sec  = (uSec / 1000000);
	timeout.tv_usec = (uSec % 1000000);

	struct fd_set sds;
	FD_ZERO(&sds);
    FD_SET(sd, &sds);

	unsigned int val = select(0, &sds, 0, 0, &timeout);
	(void) val; // remove warning

	bool avail = (FD_ISSET(sd, &sds)) ? true : false;
	return avail;
}

int UDP::getData(char* data, int length)
{
	if(error)
		return 0;
	else
	{
		int bytesRx = recvfrom(sd, data, length, 0, (struct sockaddr *)&lastClient, &client_length);
		clientIsKnown = true;

		handleMultiPoint(&lastClient);
		if(bytesRx < 0)
		{
			//printf("Error: %i   ", WSAGetLastError());
		}

		return bytesRx;
	}
}

UDP_Multipoint::UDP_Multipoint(int listenPort) : UDP(listenPort)
{
	saveClientList = false;
}
UDP_Multipoint::~UDP_Multipoint()
{

}

void UDP_Multipoint::handleMultiPoint(struct sockaddr_in *newClient)
{
	if(saveClientList)
	{
		bool foundExisting = false;
		for(unsigned i = 0; i < clientList.size(); i++)
		{
			if(clientList[i].sin_port == newClient->sin_port && 
				clientList[i].sin_addr.S_un.S_addr == newClient->sin_addr.S_un.S_addr)
			{
				foundExisting = true;
				break;
			}
		}
		if(!foundExisting)
		{
			clientList.push_back(*newClient);
		}
	}
}

void UDP_Multipoint::startSavingClientList()
{
	saveClientList = true;
}

void UDP_Multipoint::clearClientList()
{
	clientList.clear();
}

int UDP_Multipoint::getClientCount()
{
	return (int)clientList.size();
}

void UDP_Multipoint::distributeData(char* data, int length)
{
	struct sockaddr_in txClient;

	for(unsigned int i = 0; i < clientList.size(); i++)
	{
		// IF port & IP doesn't match, then distribute.
		// This is to prevent the originating source getting its own echo back.
		if(!(clientList[i].sin_port == lastClient.sin_port && 
				clientList[i].sin_addr.S_un.S_addr == lastClient.sin_addr.S_un.S_addr))
		{
			txClient = clientList[i];
			sendto(sd, data, length, 0, (struct sockaddr *)&txClient, client_length);
		}
	}
}

void UDP_Multipoint::sendDataToAllClients(char* data, int length)
{
	struct sockaddr_in txClient;

	for(unsigned int i = 0; i < clientList.size(); i++)
	{
		txClient = clientList[i];
		sendto(sd, data, length, 0, (struct sockaddr *)&txClient, client_length);
	}
}
