/*
 * hooks.c
 *
 *  This file contains FreeRTOS Hooks
 */
#include <stdio.h>
#include <FreeRTOS.h>
#include "task.h"

void vApplicationIdleHook( void )
{
    const unsigned long ulMSToSleep = 5;

    /* Sleep to reduce CPU load, but don't sleep indefinitely in case there are
    tasks waiting to be terminated by the idle task. */
    Sleep( ulMSToSleep );
}

void vApplicationTickHook(void)
{

}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
    /* Can be implemented if required, but probably not required in this
    environment and running this demo. */
    printf("Malloc Allocation has Failed!\n");
    while(1);
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook(TaskHandle_t *pxTask, signed char *pcTaskName )
{
    /* Can be implemented if required, but not required in this
    environment and running this demo. */
    printf("Stack Overflow has been detected!\n");
    printf("Task name that overflowed the stack: %s\n", (char*)pcTaskName);
    while(1);
}
