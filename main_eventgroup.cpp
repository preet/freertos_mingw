#include <stdio.h>
#include <stdint.h>

#include "network/tcp.hpp"
#include "scheduler_task.hpp"

/* Kernel includes. */
#include <FreeRTOS.h>
#include "task.h"
#include "queue.h"
#include "semphr.h"

#if (configUSE_TIMERS == 1)
    #include "timers.h"
#endif


void enableFlushAfterPrintf()
{
    setvbuf(stdout, 0, _IONBF, 0);
    setvbuf(stdin, 0, _IONBF, 0);
}

#include "event_groups.h"

// ----.--AB
//         1      <-- Task1 (good task)
//        1       <-- Task2 (bad task)
//        11      <-- Watchdog task waiting for bits to set
EventGroupHandle_t task_watchdog;
const uint32_t good_task_id = (1 << 0); // 0x01
const uint32_t bad_task_id  = (1 << 1); // 0x02
const uint32_t tasks_all_bits = (1 << 0) | (1 << 1);

void good_task(void *p)
{
    while (1) {
        // Good task, so let's always set the event group
        xEventGroupSetBits(task_watchdog, good_task_id);
        vTaskDelay(1000);
    }
}

void bad_task(void *p)
{
    int x = 0;

    while (1) {
        ++x;

        if (x >= 3 && x <= 6) {
            // Stop setting the task bit at the task_watchdog event group
        }
        else {
            xEventGroupSetBits(task_watchdog, bad_task_id);
        }

        vTaskDelay(1000);
    }
}

void sw_watchdog(void *p)
{
    while (1) {
        uint32_t result = xEventGroupWaitBits(task_watchdog, tasks_all_bits,
                                                pdTRUE, // Clear bits on exit
                                                pdTRUE, // Wait for all bits to be set
                                                2000
                                                );

        if (result == tasks_all_bits){
            puts("System is healthy...");
        }
        else {
            if (!(result & good_task_id)) {
                puts("Good task stopped responding...");
            }
            if (!(result & bad_task_id)) {
                puts("Bad task stopped responding...");
            }
        }
    }
}

int main_event_group(void)
{
    enableFlushAfterPrintf();

    task_watchdog = xEventGroupCreate();
    xTaskCreate(good_task, "good",          1000, NULL, 1, NULL);
    xTaskCreate(bad_task,  "bad ",          1000, NULL, 1, NULL);
    xTaskCreate(sw_watchdog, "sw_watchdog", 1000, NULL, 2, NULL);
    vTaskStartScheduler();

    return 0;
}
